# quiz

<img src="https://gitlab.com/Descrypt/quiz/-/raw/main/screenshot_question.png"
     alt="Screenshot of a question as displayed in quiz."
     width="50%"
     height="50%"
/>

`quiz` is a simple quiz tool.

The application uses a simple Flask web server to host an easy to use quizing tool.

> **NOTE:** `quiz` requires a quiz files directory to function.
> Consult the [setup](#setup) section for more information.

## Quickstart

```bash
python3 quiz.py 
```

Now point your web browser to [http://localhost:5005](http://localhost:5005).

## Table of contents

[TOC]

## Installation and requirements

### Requirements

[Python3](https://www.python.org/downloads/) with [pip](https://pip.pypa.io/en/stable/installation/).

### Installation

1. Clone this repository to your desired location.
2. Run `pip install -r requirements.txt` from the `quiz` directory *or* simply run `pip install flask`.

## Setup

### Quiz files directory

`quiz` requires a quiz files directory to function.
This directory holds all relevant quiz data, most notably: questions.

When no directory is specified with the `-d` command line option, 
quiz will look for a directory named `.quiz` in the user's home directory.

In other words, the default quiz files directory path is: `~/.quiz`.

### Questions directory

Within the quiz directory should be a directory named `questions`.

### Categories

Add a new directory to the `questions` directory to create a category.

### Questions

Add a text file to a category directory to create a questions file.

### Question file formatting

File contents should be formatted in a specific way.

#### Question

Adding a new question is a simple matter of typing out the new question.
For a multiple choice quiz, add each possible answer on a new line.

#### Answer

Each question is closed by adding a new line containing:

```text
Correct Answer: [ANSWER].
```

Replace `[ANSWER]` with the answer to the question.

> Note the use of capitals in Correct and Answer!

> Tip: numbering or using letters for multiple choice answers makes
> it much easier to simply refer to their number/letter instead of
> having to type an answer twice.

#### Closing dashes

Finally, all questions are closed with a triple dash:

```text
---
```

#### Background information

Adding any text between the line starting with Correct Answer and `---`
will be processed as background information.

Background information is optional content that is hidden by default.
The text can be shown at any time by clicking the Background button.
This button only shows up when background information is available.

#### Example: regular question

```text
What is Python?
Correct Answer: A programming language.
Python is a programming language.
---
```

#### Example: multiple choice question

```text
Which of the following are web browsers?
A. Apple's Safari
B. Microsoft's Windows
C. GNU/Linux
D. Google's Chrome
Correct Answer: A and D
Apple's Safari and Google's Chrome are (families of) web browsers.
Microsoft's Windows and GNU/Linux are families of operating systems.
---
```

## Usage

Run as:

```
python3 quiz.py
```

Now point your web browser to [http://localhost:5005](http://localhost:5005).

### Command line options:

For available command line options run:

```
python3 quiz.py -h
```

- `-d`: Specify a quiz files directory
- `-c`: Specify a custom CSS file
- `-p`: Specify a port number

## License

Copyright 2022 [Descrypt](https://descrypt.nl)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
