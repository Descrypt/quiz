"""
   Copyright 2022 Descrypt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


from pathlib import Path
from os.path import isdir, isfile


def parse_questions_dir(questions_dir):
    """
    Return a dict of question directories and their contents
    This is used to generate the applications landing page overview.
    """
    all_files = {}
    for path in Path(questions_dir).iterdir():
        if isdir(path):
            dir_name = str(path).split('/')[len(str(path).split('/'))-1]
            all_files.update({dir_name: []})
            for sub_path in Path(path).iterdir():
                if isfile(sub_path):
                    file_name = str(sub_path).split('/')[
                        len(str(sub_path).split('/'))-1
                    ]
                    all_files[dir_name].append(file_name)
    return all_files
