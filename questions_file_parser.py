"""
   Copyright 2022 Descrypt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


quiz utils

"""


def parse_questions_file(source_files, verbose=False):
    '''
    Load and parse question source files, returning a list of questions dicts.

    Source files should be contained in directories (categories).
    The directory name is displayed on the starting page.

    Source files are simple text files, stored as .txt
    Adding a new question is a simple matter of typing out the new
    question. For a multiple choice quiz, add each possible answer on
    a new line.

    Each question is closed by adding a new line containing:
    Correct Answer: [ANSWER].
    Note the use of capitals in Correct and Answer.
    Replace [ANSWER] with the answer to the question.

    Tip: numbering or using letters for multiple choice answers makes
    it much easier to simply refer to their number/letter instead of
    having to type an answer twice.

    Finally, all questions are closed with a triple dash:
    ---

    Adding any text between the line starting with Correct Answer and ---
    will be processed as background information.

    Background information is optional content that is hidden by default.
    The text can be shown at any time by clicking the Background button.
    This button only shows up when background information is available.

    Example of a regular question:

    //example start
    What is Python?
    Correct Answer: A programming language.
    Python is a programming language.
    ---
    //example end

    Example of a multiple choice question:

    //example start
    Which of the following are web browsers?
    A. Apple's Safari
    B. Microsoft's Windows
    C. GNU/Linux
    D. Google's Chrome
    Correct Answer: A and D
    Apple's Safari and Google's Chrome are (families of) web browsers.
    Microsoft's Windows and GNU/Linux are families of operating systems.
    ---
    //example end
    '''

    # Load all lines from all specified sources into a single list of lines

    all_lines = []
    for source_file in source_files:
        if verbose:
            print('Reading from', source_file)
        with open(source_file) as _f:
            lines = _f.readlines()
            all_lines = all_lines + lines

    # Parse the supplied source files, adding to the questions list

    questions = []

    mode = 'q'  # mode is q (question) or b (background)
    question = {'question': '', 'answer': '', 'background': ''}

    for line in all_lines:
        if mode == 'q':
            if line.startswith('Correct Answer: '):
                question['answer'] = line
                mode = 'b'
            else:
                question['question'] = question['question'] + line
        elif mode == 'b':
            if line.startswith('---'):
                questions.append(question)
                question = {'question': '', 'answer': '', 'background': ''}
                mode = 'q'
            else:
                question['background'] = question['background'] + line

    if verbose:
        print('Loaded', len(questions), 'questions')

    return questions
