"""
   Copyright 2022 Descrypt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


from argparse import ArgumentParser, RawDescriptionHelpFormatter


def parse_arguments():
    """Parse command line arguments"""
    description = (
        'Quiz yourself or others with this quizing tool.'
    )

    epilog = (
        '\n--- Quiz files directory ---'
        '\n'
        'quiz requires a quiz files directory to function.'
        '\n'
        'This directory holds all relevant quiz data, most notably: questions.'
        '\n'
        '\nWhen no directory is specified with the -d option, quiz will look'
        '\n'
        "for a directory named \".quiz\" in the user's home directory."
        '\n'
        'In other words, the default quiz files directory path is: ~/.quiz'
        '\n'
        '\n--- Questions directory ---'
        '\n'
        'Within the quiz directory should be a directory named "questions".'
        '\n'
        '\n--- Categories ---'
        '\n'
        'Add a new directory to the questions directory to create a category.'
        '\n'
        '\n--- Questions ---'
        '\n'
        'Add a text file to a category directory to create a questions file.'
        '\n'
        'Consult the manual (README.md) for more on question file formatting!'
        '\n'
        '\n--- Manual and source ---'
        '\n'
        'https://gitlab.com/Descrypt/quiz'
        '\n'
        '\n© Descrypt 2022'
        '\n'
        'https://descrypt.nl'
    )

    parser = ArgumentParser(
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "-d",
        "--dir",
        default='~/.quiz',
        help="quiz files directory file path (default: ~/.quiz)",
    )
    parser.add_argument(
        "-p",
        "--port",
        # action="store_true",
        default=5005,
        help="set the port to be used for the web server (default: 5005)",
    )
    parser.add_argument(
        "-c",
        "--css",
        # action="store_true",
        default='style.css',
        help="specify the CSS file to be used (default: style.css)",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="increase output verbosity",
    )
    args = parser.parse_args()

    return args.dir, args.port, args.css, args.verbose
