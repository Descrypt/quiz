"""
   Copyright 2022 Descrypt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


from flask import Flask, render_template, request

from questions_file_parser import parse_questions_file
from questions_dir_parser import parse_questions_dir


def setup_app(stylesheet, questions_dir, verbose):
    """Returns a fully set up Flask app object."""
    app = Flask(__name__)
    app.all_files = {}
    app.source_files = []
    app.questions = []

    @app.route('/', methods=['GET', 'POST'])
    def root():  # pylint: disable=unused-variable
        app.all_files = parse_questions_dir(questions_dir)
        if request.method == 'POST':
            app.source_files = request.form.getlist('questions')
            app.questions = parse_questions_file(app.source_files, verbose)
            return render_template(
                'frontpage.html',
                stylesheet=stylesheet,
                questions_dir=questions_dir,
                all_files=app.all_files,
                source_files=app.source_files,
                source_files_count=len(app.source_files),
                questions=app.questions,
                questions_count=len(app.questions)
            )

        return render_template(
            'frontpage.html',
            stylesheet=stylesheet,
            questions_dir=questions_dir,
            all_files=app.all_files,
            source_files=app.source_files,
            source_files_count=len(app.source_files),
            questions=app.questions,
            questions_count=len(app.questions)
        )

    @app.route('/questions/<index>')
    def questions(index):  # pylint: disable=unused-variable
        if index.isdigit():
            if int(index) not in range(1, len(app.questions)+1):
                return 'Error, index out of range'
            return render_template(
                'question.html',
                question_number=int(index),
                question_count=len(app.questions),
                stylesheet=stylesheet,
                question=app.questions[int(index)-1]['question'].split('\n'),
                answer=app.questions[int(index)-1]['answer'],
                background=app.questions[int(index)-1]['background'].split(
                    '\n')
            )

        return 'Error, expected a number, got ' + index

    return app
