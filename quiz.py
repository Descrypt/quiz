"""
   Copyright 2022 Descrypt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


Consult README.md for more information.
"""


from os.path import expanduser, isdir
from sys import path

from app import setup_app
from argument_parser import parse_arguments


def main():
    "Quiz boot and main loop"

    # Parse startup arguments
    quiz_dir, port, css, verbose = parse_arguments()

    # Load CSS file contents
    if css == 'style.css':
        css = f'{path[0]}/{css}'
    with open(css) as _f:
        stylesheet = _f.read()

    # Test quiz directory path
    if quiz_dir.startswith('~'):
        source_files = f"{expanduser('~')}{quiz_dir[1:]}"
    if not isdir(source_files):
        print(
            'Please create the required directory:\n',
            source_files
        )
        return

    # Look for questions directory at source
    questions_dir = f'{source_files}/questions'
    if not isdir(questions_dir):
        print(
            'Please create the required questions directory:\n',
            questions_dir
        )
        return

    # Init the Flask app
    app = setup_app(stylesheet, questions_dir, verbose)

    app.run(port=port)


if __name__ == '__main__':
    main()
